#include <Arduino.h>

// ***
// Arduino pin numbers
const int SW_pin = 2; // Joystick - digital pin connected to switch output
const int X_pin = 0; // Joystick - analog pin connected to X output
const int Y_pin = 1; // Joystick - analog pin connected to Y output

int latch = 9;  //74HC595  pin 9 STCP
int clock = 10; //74HC595  pin 10 SHCP
int data = 8;   //74HC595  pin 8 DS

int sel0 = 4;
int sel1 = 5;
int sel2 = 6;
int sel3 = 7;

// ***
// Variables for printing the 4 digits at 60Hz one after the other
int currentPin = 0;
int pinList[] = {sel0, sel1, sel2, sel3};

unsigned char table[] =
        {0x3f, 0x06, 0x5b, 0x4f, 0x66, 0x6d, 0x7d, 0x07, 0x7f, 0x6f,    // 0 to 9
         0xbf, 0x86, 0xdb, 0xcf, 0xe6, 0xed, 0xfd, 0x87, 0xff, 0xef,    // 0 to 9 with . (0. to 9.)
         0x79}; // 'E', used for Error
//         0x77, 0x7c, 0x39, 0x5e, 0x79, 0x71, 0x00};

bool displayOn = true;

// Time variables

byte currentHour = 6;
byte currentMinute = 42;
int currentMinuteOfDay = 6 * 60 + 42;

// ***
// Timings

unsigned long currentMillis = 0;

unsigned long displayDelay = 2; // ms
unsigned long displayLastMillis = 0;

unsigned long timeUpdateLastMillis = 0;

unsigned long timeSettingDelay = 200;  // ms = 0.2s
unsigned long timeSettingLastMillis = 0;

unsigned long BlinkingDelay = 200;  // ms = 0.2s
unsigned long BlinkingLastMillis = 0;

// ***
// Time Offset Setting

bool timeSettingState = false;
bool swButtonPressed = false;

// Functions skeletons

void Display(unsigned char num);

void WriteNextDigit(int hours, int minutes);

// Main code

__attribute__((unused)) void setup() {

    // Pins for the 7-digits
    pinMode(latch, OUTPUT);
    pinMode(clock, OUTPUT);
    pinMode(data, OUTPUT);

    pinMode(sel0, OUTPUT);
    pinMode(sel1, OUTPUT);
    pinMode(sel2, OUTPUT);
    pinMode(sel3, OUTPUT);

    digitalWrite(sel0, HIGH);
    digitalWrite(sel1, HIGH);
    digitalWrite(sel2, HIGH);
    digitalWrite(sel3, HIGH);

    // Pins for the Joystick
    pinMode(SW_pin, INPUT);
    digitalWrite(SW_pin, HIGH);

    // Serial
    Serial.begin(9600);
    Serial.print("Hello World!\n");
    Serial.print(digitalRead(SW_pin));

}

void Display(unsigned char num) {
    digitalWrite(latch, LOW);
    shiftOut(data, clock, MSBFIRST, table[num]);
    digitalWrite(latch, HIGH);
}

void WriteNextDigit(int hours, int minutes) {

    byte prevPin = currentPin;
    currentPin = (currentPin + 1) % 4;

    unsigned char num;
    switch (currentPin) {
        case 0:
            num = hours / 10;
            break;

        case 1:
            num = hours % 10;
            num += 10;  // We add 10 to get the numbers with a dot
            break;

        case 2:
            num = minutes / 10;
            break;

        case 3:
            num = minutes % 10;
            break;

        default:
            num = 20;  // This would print 'E'
            break;
    }

    // If num is over 20, it would be out of list. So we bring it back to 20, which is 'E'
    if (num > 20) num = 20;

    digitalWrite(pinList[prevPin], HIGH);
    Display(num);
    digitalWrite(pinList[currentPin], LOW);
}

void setDisplayOn(bool _displayOn) {
    displayOn = _displayOn;

    // If display is set to off, we switch off all the digits
    if (!displayOn) {
        digitalWrite(sel0, HIGH);
        digitalWrite(sel1, HIGH);
        digitalWrite(sel2, HIGH);
        digitalWrite(sel3, HIGH);
    }
}

void UpdateHour() {
    currentMinuteOfDay %= 24 * 60;
    if (currentMinuteOfDay < 0)
    {
        currentMinuteOfDay += 24 * 60;
    }

    currentMinute = currentMinuteOfDay % 60;
    currentHour = (currentMinuteOfDay / 60) % 24;
}

void ModifyTimeOffsetWithJoystick() {
    int x_coord = analogRead(X_pin);    // 0 to 1024
    int y_coord = analogRead(Y_pin);    // 0 to 1024

    int minutesDelta = (x_coord * 5) / 1024 - 2;
    int hoursDelta = ((y_coord * 3) / 1024) - 1;

    currentMinuteOfDay += 60 * hoursDelta + minutesDelta;
}

__attribute__((unused)) void loop() {

    currentMillis = millis();

    // *** Changing time ***
    // If joystickButton pressed and was not already pressed
    if (digitalRead(SW_pin) == LOW and !swButtonPressed) {
        timeSettingState = !timeSettingState;
        swButtonPressed = true;

        setDisplayOn(!timeSettingState);
    }
    // Once button is released, we update the bool
    if (swButtonPressed and digitalRead(SW_pin) == HIGH) {
        swButtonPressed = false;
    }

    // If we are in timeSetting state, every 200ms we will change the time offset
    if (timeSettingState and currentMillis - timeSettingLastMillis > timeSettingDelay) {
        timeSettingLastMillis = currentMillis;

        ModifyTimeOffsetWithJoystick();
        UpdateHour();
    }

    // If we are in timeSetting State, we make the display blink
    if (timeSettingState and currentMillis - BlinkingLastMillis > BlinkingDelay) {
        BlinkingLastMillis = currentMillis;
        setDisplayOn(!displayOn);

        // The blinking is 1s on and 0.1s off, so the delay has to be updated based on displayOn value
        BlinkingDelay = displayOn ? 500 : 100;
    }

    // *** Hour managing ***
    // Updating hour every minute
    if (currentMillis - timeUpdateLastMillis > 60000) {
        timeUpdateLastMillis += 60000;

        currentMinuteOfDay++;

        UpdateHour();
    }

    // Printing numbers
    if (displayOn and currentMillis - displayLastMillis > displayDelay) {
        displayLastMillis = currentMillis;

        WriteNextDigit(currentHour, currentMinute);
    }
}